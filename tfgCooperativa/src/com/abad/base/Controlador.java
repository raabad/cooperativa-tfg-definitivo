package com.abad.base;

import com.abad.gui.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.*;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Clase en el que damos funcionalidad al programa de gestión.
 */
public class Controlador implements ActionListener, ListSelectionListener, KeyListener {
    private Vista vista;
    private Modelo modelo;
    private RegistroUsuarios registroUsuarios;
    private Acceso acceso;
    private SplashScreen splashScreen;
    boolean passFail;
    boolean userFail;
    String nombreUsuario;
    CambioContr cambioContr;
    Map<String,Object> parametros;
    JasperPrint jasperPrint;
    JasperViewer jv;
    Connection conexion1 = null;
    JasperReport report1 = null;

    /**
     * constructor de la clase controlador
     * @param vista
     * @param modelo
     * @param registroUsuarios
     * @param acceso
     */

    public Controlador(Vista vista, Modelo modelo,RegistroUsuarios registroUsuarios,Acceso acceso) {
        this.vista=vista;
        this.modelo=modelo;
        this.registroUsuarios=registroUsuarios;
        this.acceso=acceso;
        splashScreen=new SplashScreen(modelo);
        splashScreen.run();
        acceso.frame.setVisible(true);


        userFail = true;
        passFail = false;
        addActionListenner(this);
        addListSelecctionListenner(this);
        addKeyListenner(this);
/**
 * actualizamos las listas y combos box del programa de gestion
 */
        refrescaUser();
        refrescarAgr();
        refrescarClientes();
        refrescarProductos();
        refrescarMaq();
        refrescarEmpleados();
        refrescarCbAgrProd((ArrayList<Agricultor>) modelo.getAgricultor());
        refrescarCbAgrCliente((ArrayList<Agricultor>) modelo.getAgricultor());
        refrescarCbAgrEmp((ArrayList<Agricultor>) modelo.getAgricultor());
        refrescarCbAgrMaquinaria((ArrayList<Agricultor>) modelo.getAgricultor());




    }

    public Controlador() {

    }


    /**
     * añadimos los key listenner a los componentes que les sea necesario
     * @param listener
     */
    private void addKeyListenner(KeyListener listener) {
        vista.textBuscarAgr.addKeyListener(listener);
        vista.textBuscarMaq.addKeyListener(listener);
        vista.textBuscarCl.addKeyListener(listener);
        vista.textBuscarEmp.addKeyListener(listener);
        vista.textBuscarProd.addKeyListener(listener);

    }

    /**
     * añadimos los listSelectionListener a las listas para seleccionar los datos de las listas
     * @param listener
     */
    private void addListSelecctionListenner(ListSelectionListener listener) {
        vista.listProd.addListSelectionListener(listener);
        vista.listMaq.addListSelectionListener(listener);
        vista.listEmp.addListSelectionListener(listener);
        vista.listClientes.addListSelectionListener(listener);
        vista.listAgricultor.addListSelectionListener(listener);
        registroUsuarios.listUsuariosReg.addListSelectionListener(listener);

    }

    /**
     * añadimos los action listener para darles funciones a los botones
     * @param listener
     */
    private void addActionListenner(ActionListener listener) {
        vista.btnAltaProd.addActionListener(listener);
        vista.btnModProd.addActionListener(listener);
        vista.btnBajaProd.addActionListener(listener);
        vista.btnAltaAgr.addActionListener(listener);
        vista.btnModAgr.addActionListener(listener);
        vista.btnEliminarAgr.addActionListener(listener);
        vista.btnAltaCl.addActionListener(listener);
        vista.btnModCl.addActionListener(listener);
        vista.btnBajaCl.addActionListener(listener);
        vista.btnAltaEmp.addActionListener(listener);
        vista.btnModEmp.addActionListener(listener);
        vista.btnBajaEmp.addActionListener(listener);
        vista.btnAltaMaq.addActionListener(listener);
        vista.btnModMaq.addActionListener(listener);
        vista.btnEliminarMaq.addActionListener(listener);
        registroUsuarios.btnAltaUsReg.addActionListener(listener);
        registroUsuarios.btnBajaUsReg.addActionListener(listener);
        registroUsuarios.btnModUsReg.addActionListener(listener);
        acceso.btnAcceder.addActionListener(listener);
        vista.itemAdmin.addActionListener(listener);
        vista.itemContrasenya.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.btnInformeCliente.addActionListener(listener);
        vista.btnInformeEmpleados.addActionListener(listener);
        vista.btnInformeProductos.addActionListener(listener);
        vista.btnInformeMaq.addActionListener(listener);
        vista.btnInformeAgricultor.addActionListener(listener);

    }

    /**
     * construimos un switch case en el metodo para darle las funcionalidades correspondientes a cada boton
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        String action=e.getActionCommand();

        switch (action){
            case "AltaAgr":
             modelo.altaAgricultor(vista.textNombreAgr.getText(),
                     vista.textDireccionAgr.getText(),Integer.parseInt(vista.textTelfAgr.getText()),Timestamp.valueOf(vista.dpAgr.getDateTimePermissive()),
               vista.textEmailAgr.getText(),vista.textDniAgr.getText(),Integer.parseInt(vista.textSSAgr.getText()));

                break;

            case "ModificarAgr":

                modificarAgricultor();


                break;
            case "BajaAgr":
                eliminarAgricultor();

                break;
            case "AltaEmp":
                modelo.altaEmpleados(vista.textNombreEmp.getText(),vista.textApellidosEmp.getText(),vista.textDniEmp.getText(),
                        vista.textEMailEmp.getText(),Timestamp.valueOf(vista.dpEmp.getDateTimePermissive()),Integer.parseInt(vista.textFieldExperienciaEmp.getText()),
                        vista.textTelfEmp.getText(), (Agricultor) vista.cbAgricultorEmp.getSelectedItem());


                break;
            case "ModificarEmp":
                modificarEmpleado();

                break;

            case "BajaEmp":
                eliminarEmpleados();

                break;

            case "AltaMaq":
                modelo.altaMaquinaria(vista.textModeloMaq.getText(),vista.textMarcaMaq.getText(),vista.textMatriculaMaq.getText(), (String) vista.cbCombustibleMaq.getSelectedItem(),
                       Timestamp.valueOf(vista.dpMaq.getDateTimePermissive()) ,vista.textAveriaMaq.getText(),vista.textAreaMaq.getText(),
                        (Agricultor) vista.cbAgricultorMaq.getSelectedItem());

                break;

            case "ModificarMaq":
                modificarMaq();

                break;

            case "EliminarMaq":
                eliminarMaq();

                break;

            case "AltaCl":
                modelo.altaClientes(vista.textNombreCliente.getText(), vista.textApellidosCliente.getText(),vista.textDniCliente.getText(),Integer.parseInt(vista.textTelfCliente.getText()),
                        Timestamp.valueOf(vista.dpCliente.getDateTimePermissive()),vista.textDomicilioCliente.getText(),
                        vista.textCodCliente.getText(),(Agricultor) vista.cbAgricultorClient.getSelectedItem());

                break;

            case "ModificarCl":
                modificarCliente();

                break;

            case "BajaCl":
                eliminarCliente();


                break;

            case "AltaProd":
                modelo.altaProductos(vista.textNomProd.getText(),Timestamp.valueOf(vista.dpProd.getDateTimePermissive()),Float.parseFloat(vista.textPrecioProd.getText()),
                        vista.textAreaDescrProd.getText(),Float.parseFloat(vista.textHumedadProd.getText()),
                        Integer.parseInt(vista.textCantProd.getText()),(Agricultor)vista.cbAgricultorProd.getSelectedItem());

                break;

            case "ModificarProd":
                modificarProductos();


                break;

            case "BajaProd":
                eliminarProductos();


                break;

            case "AltaRegUs":
                modelo.altaUsuario(registroUsuarios.textUserRegistro.getText(), String.valueOf(registroUsuarios.passwordContrRegistro.getPassword()),String.valueOf(registroUsuarios.dcbmRol.getSelectedItem()));

                break;

            case "ModificarRegUs":
                modificarUsuario();

                break;

            case "BajaRegUs":
                eliminarUsuarios();

                break;


            case "ACCEDERInicio"  :
/**
 * boton para comprobar el inicio y sus permisos segun el rol del usuario
 */
                userFail = true;
                if (!(acceso.textUsuarioInicio.getText().isEmpty() || acceso.textUsuarioInicio.getText().equals(" ")) && !(acceso.passwordInicio.getText().isEmpty() || acceso.passwordInicio.getPassword().equals(" "))) {
                    for (Usuario usuario : modelo.getUsuarios()) {
                        if (acceso.textUsuarioInicio.getText().equals(usuario.getNameUser())) {
                            userFail = false;
                            passFail = false;
                            if (acceso.passwordInicio.getText().equals(usuario.getPass())) {
                                vista.frame.setVisible(true);
                                acceso.frame.dispose();
                                nombreUsuario = usuario.getNameUser();
                                if (usuario.getRol() == 2) {
                                    vista.menuAdmin.setVisible(false);
                                    vista.menuAdmin.setEnabled(false);
                                } else if (usuario.getRol() == 3) {
                                    restringirPermisos();
                                }
                            } else {
                                passFail = true;
                            }
                        }
                    }
                    if (passFail == true && userFail == false) {
                        JOptionPane.showMessageDialog(null, "La contraseña introducida no es correcta, revisela", "Error al Acceder", JOptionPane.ERROR_MESSAGE);
                    }
                }
                if (userFail == true) {
                    JOptionPane.showMessageDialog(null, "El usuario introducido no es correcto, reviselo, se diferencian las mayusculas", "Error al Acceder", JOptionPane.ERROR_MESSAGE);
                }
                if (acceso.textUsuarioInicio.getText().isEmpty() || acceso.textUsuarioInicio.getText().equals(" ")) {
                    JOptionPane.showMessageDialog(null, "Introduzca nombre de usuario, este campo no puede esta vacio", "Error al Acceder", JOptionPane.ERROR_MESSAGE);
                }
                if (acceso.passwordInicio.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Introduzca la contraseña, este campo no puede esta vacio", "Error al Acceder", JOptionPane.ERROR_MESSAGE);
                }

                    break;


            case "Cambiar Contr":
                System.out.println("entra");
                for (Usuario usuario:modelo.getUsuarios()
                     ) {
                   if (usuario.getNameUser().equals(nombreUsuario)){
                        cambioContr=new CambioContr(usuario,modelo);
                   }
                }


                break;

            case "Registrar Usuarios":

                registroUsuarios.frame.setVisible(true);


                break;

            case "Salir":

                modelo.desconectar();
                vista.frame.dispose();

                break;

            case "informeCliente":

                ImageIcon logo = new ImageIcon("Imagenes/IconoFondoNegro.png");

                try {
                    Class.forName("com.mysql.jdbc.Driver");
                } catch (ClassNotFoundException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                try {
                    conexion1 = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/cooperativa", "root", "");
                } catch (SQLException e2) {
                    e2.printStackTrace();
                }
                JasperReport report2 = null;
                try {
                    report2 = (JasperReport) JRLoader.loadObjectFromFile("informes/reportCliente.jasper");
                    parametros = new HashMap<String, Object>();
                    parametros.put("titulo", "INFORME CLIENTES");
                    parametros.put("NOMBRE_PARAMETRO", this.getClass().getResourceAsStream(String.valueOf(logo)));
                    jasperPrint = JasperFillManager.fillReport(report2, parametros, conexion1);

                    jv = new JasperViewer(jasperPrint, false);
                    jv.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                    jv.setVisible(true);



                } catch (JRException e1) {
                    e1.printStackTrace();
                    System.out.println("Vaya, no se puede cargar el fichero");
                }


                break;

            case "InformeEmpleados":


                ImageIcon logo1 = new ImageIcon("Imagenes/IconoFondoNegro.png");

                try {
                    Class.forName("com.mysql.jdbc.Driver");
                } catch (ClassNotFoundException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                try {
                    conexion1 = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/cooperativa", "root", "");
                } catch (SQLException e2) {
                    e2.printStackTrace();
                }
                JasperReport report = null;
                try {
                    report = (JasperReport) JRLoader.loadObjectFromFile("informes/reportEmpleados.jasper");
                    parametros = new HashMap<String, Object>();
                    parametros.put("titulo", "INFORME EMPLEADOS");
                    parametros.put("NOMBRE_PARAMETRO", this.getClass().getResourceAsStream(String.valueOf(logo1)));
                    jasperPrint = JasperFillManager.fillReport(report, parametros, conexion1);

                    jv = new JasperViewer(jasperPrint, false);
                    jv.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                    jv.setVisible(true);



                } catch (JRException e1) {
                    e1.printStackTrace();
                    System.out.println(" no se puede cargar el fichero");
                }





                break;

            case "Informe Productos":

                ImageIcon logo0 = new ImageIcon("Imagenes/IconoFondoNegro.png");

                try {
                    Class.forName("com.mysql.jdbc.Driver");
                } catch (ClassNotFoundException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                try {
                    conexion1 = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/cooperativa", "root", "");
                } catch (SQLException e2) {
                    e2.printStackTrace();
                }
                JasperReport report3 = null;
                try {
                    report3 = (JasperReport) JRLoader.loadObjectFromFile("informes/reportProductos.jasper");
                    parametros = new HashMap<String, Object>();
                    parametros.put("titulo", "INFORME PODUCTOS");
                    parametros.put("NOMBRE_PARAMETRO", this.getClass().getResourceAsStream(String.valueOf(logo0)));
                    jasperPrint = JasperFillManager.fillReport(report3, parametros, conexion1);

                    jv = new JasperViewer(jasperPrint, false);
                    jv.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                    jv.setVisible(true);



                } catch (JRException e1) {
                    e1.printStackTrace();
                    System.out.println(" no se puede cargar el fichero");
                }





                break;
/**
 * Generamos informes de todos los apartados
 */

            case"Informe Maquinaria":



                ImageIcon logo5 = new ImageIcon("Imagenes/IconoFondoNegro.png");

                try {
                    Class.forName("com.mysql.jdbc.Driver");
                } catch (ClassNotFoundException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                try {
                    conexion1 = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/cooperativa", "root", "");
                } catch (SQLException e2) {
                    e2.printStackTrace();
                }
                JasperReport report5 = null;
                try {
                    report5 = (JasperReport) JRLoader.loadObjectFromFile("informes/reportMaquinaria.jasper");
                    parametros = new HashMap<String, Object>();
                    parametros.put("titulo", "INFORME MAQUINARIA");
                    parametros.put("NOMBRE_PARAMETRO", this.getClass().getResourceAsStream(String.valueOf(logo5)));
                    jasperPrint = JasperFillManager.fillReport(report5, parametros, conexion1);

                    jv = new JasperViewer(jasperPrint, false);
                    jv.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                    jv.setVisible(true);



                } catch (JRException e1) {
                    e1.printStackTrace();
                    System.out.println(" no se puede cargar el fichero");
                }



                break;




            case "Informe Agricultor":


                ImageIcon logo6 = new ImageIcon("Imagenes/IconoFondoNegro.png");

                try {
                    Class.forName("com.mysql.jdbc.Driver");
                } catch (ClassNotFoundException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                try {
                    conexion1 = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/cooperativa", "root", "");
                } catch (SQLException e2) {
                    e2.printStackTrace();
                }
                JasperReport report6 = null;
                try {
                    report6 = (JasperReport) JRLoader.loadObjectFromFile("informes/reportAgricultor.jasper");
                    parametros = new HashMap<String, Object>();
                    parametros.put("titulo", "INFORME AGRICULTOR");
                    parametros.put("NOMBRE_PARAMETRO", this.getClass().getResourceAsStream(String.valueOf(logo6)));
                    jasperPrint = JasperFillManager.fillReport(report6, parametros, conexion1);

                    jv = new JasperViewer(jasperPrint, false);
                    jv.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                    jv.setVisible(true);



                } catch (JRException e1) {
                    e1.printStackTrace();
                    System.out.println(" no se puede cargar el fichero");
                }

                break;




        }


        /**
         * actualizamos todos los datos del programa de  gestion
         */
        borrarDatosRegUsuario();
        borrarDatosAgr();
        borrarDatosCl();
        borrarDatosEmp();
        borrarDatosMaq();
        borrarDatosProds();
        borrarDatosAceso();
        refrescaUser();
        refrescarAgr();
        refrescarClientes();
        refrescarProductos();
        refrescarMaq();
        refrescarEmpleados();
        refrescarCbAgrProd((ArrayList<Agricultor>) modelo.getAgricultor());
        refrescarCbAgrCliente((ArrayList<Agricultor>) modelo.getAgricultor());
        refrescarCbAgrEmp((ArrayList<Agricultor>) modelo.getAgricultor());
        refrescarCbAgrMaquinaria((ArrayList<Agricultor>) modelo.getAgricultor());

    }



    /**
     * modifica un usuario
     */
    public void modificarUsuario(){

        Usuario usuario= (Usuario) registroUsuarios.listUsuariosReg.getSelectedValue();
        usuario.setNameUser(registroUsuarios.textUserRegistro.getText());
        if (registroUsuarios.dcbmRol.getSelectedItem().toString().equals("Empleado")) {
            usuario.setRol(2);
        } else {
            usuario.setRol(3);
        }
        modelo.modificarUsuario(usuario);


    }

    /**
     * elimina un usuario
     */
    public void eliminarUsuarios(){
        Usuario usuario= (Usuario) registroUsuarios.listUsuariosReg.getSelectedValue();
        modelo.eliminarUsuario(usuario);
    }

    /**
     * modifica un agricultor selecciondo
     */
    private void modificarAgricultor(){
        Agricultor agricultor= (Agricultor) vista.listAgricultor.getSelectedValue();
        agricultor.setNombre(vista.textNombreAgr.getText());
        agricultor.setDireccion(vista.textDireccionAgr.getText());
        agricultor.setTelefono(Integer.parseInt(vista.textTelfAgr.getText()));
        agricultor.setFechaNacimiento(Timestamp.valueOf(vista.dpAgr.getDateTimePermissive()));
        agricultor.setEmail(vista.textEmailAgr.getText());
        agricultor.setDni(vista.textDniAgr.getText());
        agricultor.setSs(Integer.parseInt(vista.textSSAgr.getText()));
        modelo.modificarAgricultor(agricultor);

    }

    /**
     * elimina un agricultor seleccionado
     */
    private void eliminarAgricultor(){
        Agricultor agricultor= (Agricultor) vista.listAgricultor.getSelectedValue();
        modelo.eliminarAgricultor(agricultor);
    }

    /**
     * modifica un empleado seleccionado
     */

    private void modificarEmpleado(){
        Empleados empleados= (Empleados) vista.listEmp.getSelectedValue();
        empleados.setNombre(vista.textNombreEmp.getText());
        empleados.setApellidos(vista.textApellidosEmp.getText());
        empleados.setDni(vista.textDniEmp.getText());
        empleados.setEmail(vista.textEMailEmp.getText());
        empleados.setFechaContratacion(Timestamp.valueOf(vista.dpEmp.getDateTimePermissive()));
        empleados.setExperiencia(Integer.parseInt(vista.textFieldExperienciaEmp.getText()));
        empleados.setTelefono(vista.textTelfEmp.getText());
        empleados.setAgricultor((Agricultor) vista.cbAgricultorEmp.getSelectedItem());
        modelo.modificarEmpleados(empleados);
    }

    /**
     * elimina un empleado seleccionado
     */
    private void eliminarEmpleados(){
        Empleados empleados= (Empleados) vista.listEmp.getSelectedValue();
        modelo.eliminarEmpleados(empleados);

    }

    /**
     * modifica una maquinaria seleccionada
     */
    private void modificarMaq(){
        Maquinaria maquinaria= (Maquinaria) vista.listMaq.getSelectedValue();
        maquinaria.setModelo(vista.textModeloMaq.getText());
        maquinaria.setMarca(vista.textMarcaMaq.getText());
        maquinaria.setMatricula(vista.textMatriculaMaq.getText());
        maquinaria.setCombustible((String) vista.cbCombustibleMaq.getSelectedItem());
        maquinaria.setFechaCompra(Timestamp.valueOf(vista.dpMaq.getDateTimePermissive()));
        maquinaria.setAveria(vista.textAveriaMaq.getText());
        maquinaria.setDescripcion(vista.textAreaMaq.getText());
        maquinaria.setAgricultor((Agricultor) vista.cbAgricultorMaq.getSelectedItem());
        modelo.modificarMaquinaria(maquinaria);

    }

    /**
     * elimina una maquinaria seleccionada
     */
    private void eliminarMaq(){

        Maquinaria maquinaria= (Maquinaria) vista.listMaq.getSelectedValue();
        modelo.eliminarMaquinaria(maquinaria);

    }

    /**
     * modifica un cliente seleccionado
     */
    private void modificarCliente(){
        Cliente cliente= (Cliente) vista.listClientes.getSelectedValue();
        cliente.setNombre(vista.textNombreCliente.getText());
        cliente.setApellidos(vista.textApellidosCliente.getText());
        cliente.setDni(vista.textDniCliente.getText());
        cliente.setTelefono(Integer.parseInt(vista.textTelfCliente.getText()));
        cliente.setFechaNacimiento(Timestamp.valueOf(vista.dpCliente.getDateTimePermissive()));
        cliente.setDomicilio(vista.textDomicilioCliente.getText());
        cliente.setCodigoPostal(vista.textCodCliente.getText());
        cliente.setAgricultor((Agricultor) vista.cbAgricultorClient.getSelectedItem());
        modelo.modificarClientes(cliente);
    }

    /**
     * elimina un cliente seleccionado
     */
    private void eliminarCliente(){
        Cliente cliente= (Cliente) vista.listClientes.getSelectedValue();
        modelo.eliminarClientes(cliente);
    }

    /**
     * modifica un producto seleeccionado
     */
    private void modificarProductos(){
        Productos productos= (Productos) vista.listProd.getSelectedValue();
        productos.setNombre(vista.textNomProd.getText());
        productos.setFechaCosecha(Timestamp.valueOf(vista.dpProd.getDateTimePermissive()));
        productos.setPrecio(Float.parseFloat(vista.textPrecioProd.getText()));
        productos.setDescripcion(vista.textAreaDescrProd.getText());
        productos.setHumedad(Float.parseFloat(vista.textHumedadProd.getText()));
        productos.setCantidad(Integer.parseInt(vista.textCantProd.getText()));
        productos.setAgricultor((Agricultor) vista.cbAgricultorProd.getSelectedItem());
        modelo.modificarProductos(productos);
    }

    /**
     * elimina un producto seleccionado
     */
    private void eliminarProductos(){
        Productos productos= (Productos) vista.listProd.getSelectedValue();
        modelo.eliminarProductos(productos);
    }

    /**
     * borra los datos de los campos del agricultor
     */
    private void borrarDatosAgr(){
        vista.textNombreAgr.setText("");
        vista.textDireccionAgr.setText("");
        vista.textTelfAgr.setText("");
        vista.dpAgr.setDateTimePermissive(null);
        vista.textEmailAgr.setText("");
        vista.textDniAgr.setText("");
        vista.textSSAgr.setText("");

    }

    /**
     * borra los datos de los campos del empleado
     */

    private void borrarDatosEmp(){
        vista.textNombreEmp.setText("");
        vista.textApellidosEmp.setText("");
        vista.textDniEmp.setText("");
        vista.textEMailEmp.setText("");
        vista.dpEmp.setDateTimePermissive(null);
        vista.textFieldExperienciaEmp.setText("");
        vista.textTelfEmp.setText("");

    }
    /**
     * borra los datos de los campos de la maquinaria
     */
    private void borrarDatosMaq(){
        vista.textModeloMaq.setText("");
        vista.textMarcaMaq.setText("");
        vista.textMatriculaMaq.setText("");
        vista.cbCombustibleMaq.setSelectedItem("");
        vista.dpMaq.setDateTimePermissive(null);
        vista.textAveriaMaq.setText("");
        vista.textAreaMaq.setText("");

    }

    /**
     * borra los datos de los campos del cliente
     */
    private void borrarDatosCl(){

        vista.textNombreCliente.setText("");
        vista.textApellidosCliente.setText("");
        vista.textDniCliente.setText("");
        vista.textTelfCliente.setText("");
        vista.dpCliente.setDateTimePermissive(null);
        vista.textDomicilioCliente.setText("");
        vista.textCodCliente.setText("");

    }

    /**
     * borra los datos de los campos del producto
     */
    private void borrarDatosProds(){

        vista.textNomProd.setText("");
        vista.dpProd.setDateTimePermissive(null);
        vista.textPrecioProd.setText("");
        vista.textAreaDescrProd.setText("");
        vista.textHumedadProd.setText("");
        vista.textCantProd.setText("");

    }
    /**
     * borra los datos de los campos del registro de usuarios
     */
    private void borrarDatosRegUsuario(){

        registroUsuarios.textUserRegistro.setText("");
        registroUsuarios.passwordContrRegistro.setText("");

    }
    /**
     * borra los datos de los campos del acceso
     */
    private void borrarDatosAceso(){
        acceso.textUsuarioInicio.setText("");
        acceso.passwordInicio.setText("");
    }

/**
muestra los datos del agricultor seleccionado
 */
    private void mostrarDatosAgr( Agricultor agricultor){
         agricultor= (Agricultor) vista.listAgricultor.getSelectedValue();
        vista.textNombreAgr.setText(agricultor.getNombre());
        vista.textDireccionAgr.setText(agricultor.getDireccion());
        vista.textTelfAgr.setText(String.valueOf(agricultor.getTelefono()));
        vista.dpAgr.setDateTimePermissive(agricultor.getFechaNacimiento().toLocalDateTime());
        vista.textEmailAgr.setText(agricultor.getEmail());
        vista.textDniAgr.setText(agricultor.getDni());
        vista.textSSAgr.setText(String.valueOf(agricultor.getSs()));
    }

    /**
     * muestra los datos de los empleados seleccionados
     * @param empleados
     */
    private void mostrarDatosEmp( Empleados empleados){
         empleados= (Empleados) vista.listEmp.getSelectedValue();
        vista.textNombreEmp.setText(empleados.getNombre());
        vista.textApellidosEmp.setText(empleados.getApellidos());
        vista.textDniEmp.setText(empleados.getDni());
        vista.textEMailEmp.setText(empleados.getEmail());
        vista.dpEmp.setDateTimePermissive(empleados.getFechaContratacion().toLocalDateTime());
        vista.textFieldExperienciaEmp.setText(String.valueOf(empleados.getExperiencia()));
        vista.textTelfEmp.setText(empleados.getTelefono());
        vista.cbAgricultorEmp.setSelectedItem(empleados.getAgricultor());
    }

    /**
     * muestra los datos de la maquinaria seleccionada
     * @param maquinaria
     */
    private void mostrarDatosMaq( Maquinaria maquinaria){
         maquinaria= (Maquinaria) vista.listMaq.getSelectedValue();
        vista.textModeloMaq.setText(maquinaria.getModelo());
        vista.textMarcaMaq.setText(maquinaria.getMarca());
        vista.textMatriculaMaq.setText(maquinaria.getMatricula());
        vista.cbCombustibleMaq.setSelectedItem(maquinaria.getCombustible());
        vista.dpMaq.setDateTimePermissive(maquinaria.getFechaCompra().toLocalDateTime());
        vista.textAveriaMaq.setText(maquinaria.getAveria());
        vista.textAreaMaq.setText(maquinaria.getDescripcion());
        vista.cbAgricultorMaq.setSelectedItem(maquinaria.getAgricultor());

    }

    /**
     * muestra los datos del cliente seleccionado
     * @param cliente
     */
    private void mostrarDatosCliente( Cliente cliente){
         cliente= (Cliente) vista.listClientes.getSelectedValue();
        vista.textNombreCliente.setText(cliente.getNombre());
        vista.textApellidosCliente.setText(cliente.getApellidos());
        vista.textDniCliente.setText(cliente.getDni());
        vista.textTelfCliente.setText(String.valueOf(cliente.getTelefono()));
        vista.dpCliente.setDateTimePermissive(cliente.getFechaNacimiento().toLocalDateTime());
        vista.textDomicilioCliente.setText(cliente.getDomicilio());
        vista.textCodCliente.setText(cliente.getCodigoPostal());
        vista.cbAgricultorClient.setSelectedItem(cliente.getAgricultor());

    }

    /**
     * muestra los datos de los productos seleccionados
     * @param productos
     */
    private void mostrarDatosProductos(Productos productos){
         productos= (Productos) vista.listProd.getSelectedValue();
        vista.textNomProd.setText(productos.getNombre());
        vista.dpProd.setDateTimePermissive(productos.getFechaCosecha().toLocalDateTime());
        vista.textPrecioProd.setText(String.valueOf(productos.getPrecio()));
        vista.textAreaDescrProd.setText(productos.getDescripcion());
        vista.textHumedadProd.setText(String.valueOf(productos.getHumedad()));
        vista.textCantProd.setText(String.valueOf(productos.getCantidad()));
        vista.cbAgricultorProd.setSelectedItem(productos.getAgricultor());
    }

    /**
     * muestra los datos del usuario seleccionado
     * @param usuario
     */
    private void mostrarDatosUser(Usuario usuario){
        registroUsuarios.textUserRegistro.setText(usuario.getNameUser());
        if (usuario.getRol() == 2) {
            registroUsuarios.dcbmRol.setSelectedItem("Empleado");
        } else {
            registroUsuarios.dcbmRol.setSelectedItem("Consultor");
        }
    }

    /**
     * metodo para actualizar los usuarios y añadirlos a la lista
     */
    public void refrescaUser(){
        registroUsuarios.dlmUsuarios.removeAllElements();
        for (Usuario usuario:modelo.getUsuarios()
             ) {
            if (usuario.getRol()!=1){
                registroUsuarios.dlmUsuarios.addElement(usuario);
            }

        }
    }

    /**
     * metodo para actualizar los agricultores y añadirlos a la lista
     */
    public void refrescarAgr(){
        vista.dlmAgr.removeAllElements();
        for (Agricultor agricultor:modelo.getAgricultor()
             ) {
            vista.dlmAgr.addElement(agricultor);
        }
    }
    /**
     * metodo para actualizar los clientes y añadirlos a la lista
     */
    public void refrescarClientes(){
        vista.dlmCl.removeAllElements();
        for (Cliente cliente:modelo.getCliente()
             ) {
            vista.dlmCl.addElement(cliente);
        }
    }

    /**
     * metodo para actualizar los empleados y añadirlos a la lista
     */

    public void refrescarEmpleados(){
        vista.dlmEmp.removeAllElements();
        for (Empleados empleados:modelo.getEmpleados()
        ) {
            vista.dlmEmp.addElement(empleados);
        }
    }
    /**
     * metodo para actualizar la maquinaria y añadirlos a la lista
     */
    public void refrescarMaq (){
        vista.dlmMaq.removeAllElements();
        for (Maquinaria maquinaria:modelo.getMaquinaria()
        ) {
            vista.dlmMaq.addElement(maquinaria);
        }
    }
    /**
     * metodo para actualizar los productos y añadirlos a la lista
     */
    public void refrescarProductos(){
        vista.dlmProd.removeAllElements();
        for (Productos productos:modelo.getProductos()
        ) {
            vista.dlmProd.addElement(productos);
        }
    }

    /**
     * refresca los agricultores de cada combo box
     * @param list
     */
    public void refrescarCbAgrProd(ArrayList<Agricultor>list){

            vista.dcbmAgrProd.removeAllElements();
            for (Agricultor agricultor : list) {
                vista.dcbmAgrProd.addElement(agricultor);
            }
        }

    /**
     * refresca los agricultores de cada combo box
     * @param list
     */
    public void refrescarCbAgrCliente(ArrayList<Agricultor>list){
        vista.dcbmAgrCl.removeAllElements();

        for (Agricultor agricultor :list) {
            vista.dcbmAgrCl.addElement(agricultor);
        }
    }
    /**
     * refresca los agricultores de cada combo box
     * @param list
     */
    public void refrescarCbAgrEmp(ArrayList<Agricultor>list){
        vista.dcbmAgrEmp.removeAllElements();
        for (Agricultor agricultor : list) {
            vista.dcbmAgrEmp.addElement(agricultor);
        }

    }
    /**
     * refresca los agricultores de cada combo box
     * @param list
     */
    public void refrescarCbAgrMaquinaria(ArrayList<Agricultor>list){
        vista.dcbmAgrMaq.removeAllElements();

        for (Agricultor agricultor : list) {
            vista.dcbmAgrMaq.addElement(agricultor);
        }
    }
    /**
     * refresca los agricultores de cada combo box
     * @param lista
     */
    private void refrescarDlmClienteAgr(ArrayList<Cliente> lista){
        vista.dlmClAgr.clear();
        for (Cliente cliente:lista){
            vista.dlmClAgr.addElement(cliente);
        }

    }
    /**
     * refresca los agricultores de cada combo box
     * @param lista
     */
    private void refrescarDlmEmpAgr(ArrayList<Empleados> lista){
        vista.dlmEmpAgr.clear();
        for (Empleados empleados:lista){
            vista.dlmEmpAgr.addElement(empleados);
        }

    }
    /**
     * refresca los agricultores de cada combo box
     * @param lista
     */
    private void refrescarDlmMaqAgr(ArrayList<Maquinaria> lista){
        vista.dlmMaqAgr.clear();
        for (Maquinaria maquinaria:lista){
            vista.dlmMaqAgr.addElement(maquinaria);
        }

    }
    /**
     * refresca los agricultores de cada combo box
     * @param lista
     */
    private void refrescarDlmProductosAgr(ArrayList<Productos> lista){
        vista.dlmProdAgr.clear();
        for (Productos productos:lista){
            vista.dlmProdAgr.addElement(productos);
        }

    }

    /**
     * bloque campos y botones
     */

    private void restringirPermisos() {
        vista.btnAltaMaq.setEnabled(false);
        vista.btnAltaAgr.setEnabled(false);
        vista.btnAltaEmp.setEnabled(false);
        vista.btnAltaCl.setEnabled(false);
        vista.btnAltaProd.setEnabled(false);
        vista.btnModAgr.setEnabled(false);
        vista.btnModCl.setEnabled(false);
        vista.btnModMaq.setEnabled(false);
        vista.btnModProd.setEnabled(false);
        vista.btnModEmp.setEnabled(false);
        vista.btnEliminarMaq.setEnabled(false);
        vista.btnEliminarAgr.setEnabled(false);
        vista.btnBajaEmp.setEnabled(false);
        vista.btnBajaProd.setEnabled(false);
        vista.btnBajaCl.setEnabled(false);
        vista.btnInformeAgricultor.setEnabled(false);
        vista.btnInformeMaq.setEnabled(false);
        vista.btnInformeEmpleados.setEnabled(false);
        vista.btnInformeProductos.setEnabled(false);
        vista.btnInformeCliente.setEnabled(false);
        vista.menuAdmin.setEnabled(false);
        vista.menuSalir.setEnabled(false);
        vista.menuContrasenya.setEnabled(false);
        vista.textBuscarCl.setEnabled(false);
        vista.textBuscarEmp.setEnabled(false);
        vista.textBuscarAgr.setEnabled(false);
        vista.textBuscarProd.setEnabled(false);
        vista.textBuscarAgr.setEnabled(false);
        vista.textBuscarMaq.setEnabled(false);
        vista.textNombreCliente.setEnabled(false);
        vista.textApellidosCliente.setEnabled(false);
        vista.textCodCliente.setEnabled(false);
        vista.textDomicilioCliente.setEnabled(false);
        vista.textDniCliente.setEnabled(false);
        vista.textTelfCliente.setEnabled(false);
        vista.cbAgricultorClient.setEnabled(false);
        vista.dpCliente.setEnabled(false);
        vista.textNomProd.setEnabled(false);
        vista.dpProd.setEnabled(false);
        vista.textPrecioProd.setEnabled(false);
        vista.textAreaDescrProd.setEnabled(false);
        vista.textHumedadProd.setEnabled(false);
        vista.textCantProd.setEnabled(false);
        vista.cbAgricultorProd.setEnabled(false);
        vista.textModeloMaq.setEnabled(false);
        vista.textMarcaMaq.setEnabled(false);
        vista.cbCombustibleMaq.setEnabled(false);
        vista.textAveriaMaq.setEnabled(false);
        vista.dpMaq.setEnabled(false);
        vista.textAveriaMaq.setEnabled(false);
        vista.cbAgricultorMaq.setEnabled(false);
        vista.textNombreEmp.setEnabled(false);
        vista.textApellidosEmp.setEnabled(false);
        vista.textDniEmp.setEnabled(false);
        vista.textEMailEmp.setEnabled(false);
        vista.dpEmp.setEnabled(false);
        vista.textFieldExperienciaEmp.setEnabled(false);
        vista.textTelfEmp.setEnabled(false);
        vista.cbAgricultorEmp.setEnabled(false);
        vista.textNombreAgr.setEnabled(false);
        vista.textDireccionAgr.setEnabled(false);
        vista.textTelfAgr.setEnabled(false);
        vista.dpAgr.setEnabled(false);
        vista.textEmailAgr.setEnabled(false);
        vista.textSSAgr.setEnabled(false);

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    /**
     * realizacion de busquedas dinámicas.
     * @param e
     */
    @Override
    public void keyReleased(KeyEvent e) {

        if (vista.textBuscarCl==e.getSource()){
            String textCl = vista.textBuscarCl.getText();
            if (textCl.trim().length() > 0) {
                //nuevo Model temporal
                DefaultListModel<String> tmp = new DefaultListModel();
                for (int i = 0; i < vista.dlmCl.getSize(); i++) {//recorre Model original
                    //si encuentra coincidencias agrega a model temporal
                    if (vista.dlmCl.getElementAt(i).getNombre().toLowerCase().contains(textCl.toLowerCase())) {
                        tmp.addElement(String.valueOf(vista.dlmCl.getElementAt(i)));
                    }
                }
                //agrega nuevo modelo a JList
                vista.listClientes.setModel(tmp);
            } else {//si esta vacio muestra el Model original
                vista.listClientes.setModel(vista.dlmCl);
            }
        }
        if (vista.textBuscarAgr==e.getSource()){
            String textAgr = vista.textBuscarAgr.getText();
            if (textAgr.trim().length() > 0) {
                //nuevo Model temporal
                DefaultListModel<String> tmp = new DefaultListModel();
                for (int i = 0; i < vista.dlmAgr.getSize(); i++) {//recorre Model original
                    //si encuentra coincidencias agrega a model temporal
                    if (vista.dlmAgr.getElementAt(i).getNombre().toLowerCase().contains(textAgr.toLowerCase())||vista.dlmAgr.getElementAt(i).getDireccion().toLowerCase().contains(textAgr.toLowerCase())) {
                        tmp.addElement(String.valueOf(vista.dlmAgr.getElementAt(i)));
                    }
                }
                //agrega nuevo modelo a JList
                vista.listAgricultor.setModel(tmp);
            } else {//si esta vacio muestra el Model original
                vista.listAgricultor.setModel(vista.dlmAgr);
            }
        }

        if (vista.textBuscarMaq==e.getSource()){
            String textMaquinaria = vista.textBuscarMaq.getText();
            if (textMaquinaria.trim().length() > 0) {
                //nuevo Model temporal
                DefaultListModel<String> tmp = new DefaultListModel();
                for (int i = 0; i < vista.dlmMaq.getSize(); i++) {//recorre Model original
                    //si encuentra coincidencias agrega a model temporal
                    if (vista.dlmMaq.getElementAt(i).getModelo().toLowerCase().contains(textMaquinaria.toLowerCase())||vista.dlmMaq.getElementAt(i).getMarca().toLowerCase().contains(textMaquinaria.toLowerCase())) {
                        tmp.addElement(String.valueOf(vista.dlmMaq.getElementAt(i)));
                    }
                }
                //agrega nuevo modelo a JList
                vista.listMaq.setModel(tmp);
            } else {//si esta vacio muestra el Model original
                vista.listMaq.setModel(vista.dlmMaq);
            }
        }
        if (vista.textBuscarProd==e.getSource()){
            String textProductos = vista.textBuscarProd.getText();
            if (textProductos.trim().length() > 0) {
                //nuevo Model temporal
                DefaultListModel<String> tmp = new DefaultListModel();
                for (int i = 0; i < vista.dlmProd.getSize(); i++) {//recorre Model original
                    //si encuentra coincidencias agrega a model temporal
                    if (vista.dlmProd.getElementAt(i).getNombre().toLowerCase().contains(textProductos.toLowerCase())) {
                        tmp.addElement(String.valueOf(vista.dlmProd.getElementAt(i)));
                    }
                }
                //agrega nuevo modelo a JList
                vista.listProd.setModel(tmp);
            } else {//si esta vacio muestra el Model original
                vista.listProd.setModel(vista.dlmProd);
            }
        }

        if (vista.textBuscarEmp==e.getSource()){
            String textEmp = vista.textBuscarEmp.getText();
            if (textEmp.trim().length() > 0) {
                //nuevo Model temporal
                DefaultListModel<String> tmp = new DefaultListModel();
                for (int i = 0; i < vista.dlmEmp.getSize(); i++) {//recorre Model original
                    //si encuentra coincidencias agrega a model temporal
                    if (vista.dlmEmp.getElementAt(i).getNombre().toLowerCase().contains(textEmp.toLowerCase())) {
                        tmp.addElement(String.valueOf(vista.dlmEmp.getElementAt(i)));
                    }
                }
                //agrega nuevo modelo a JList
                vista.listEmp.setModel(tmp);
            } else {//si esta vacio muestra el Model original
                vista.listEmp.setModel(vista.dlmEmp);
            }
        }

    }

    /**
     * muestra los datos de los objetos seleccionados en sus campos correspondientes
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {

        if (e.getValueIsAdjusting()) {
            if (e.getSource() == vista.listAgricultor) {

                Agricultor agricultor= (Agricultor) vista.listAgricultor.getSelectedValue();
                /*
                Ver las entidades que le pertenecen al agricultor, faltan listas y refrescares
                 */
                refrescarDlmClienteAgr(modelo.getClienteAgricultor(agricultor));
                refrescarDlmEmpAgr(modelo.getEmpleadosAgricultor(agricultor));
                refrescarDlmMaqAgr(modelo.getMaquinariaAgricultor(agricultor));
                refrescarDlmProductosAgr(modelo.getProductosAgricultor(agricultor));
                mostrarDatosAgr(agricultor);

            }

            else if (e.getSource() == vista.listEmp) {
                Empleados empleados= (Empleados) vista.listEmp.getSelectedValue();
                mostrarDatosEmp(empleados);


            }
            else if (e.getSource() == vista.listMaq) {

                Maquinaria maqSelecc = (Maquinaria) vista.listMaq.getSelectedValue();

                mostrarDatosMaq(maqSelecc);

            }
            else if(e.getSource()==vista.listProd){

                Productos productos= (Productos) vista.listProd.getSelectedValue();
                mostrarDatosProductos(productos);

            }

            else if(e.getSource()==vista.listClientes){

                Cliente cliente= (Cliente) vista.listClientes.getSelectedValue();
                mostrarDatosCliente(cliente);

            }

            else if (e.getSource()==registroUsuarios.listUsuariosReg){
                Usuario usuario= (Usuario) registroUsuarios.listUsuariosReg.getSelectedValue();
                mostrarDatosUser(usuario);
            }
        }
        }

    public Vista getVista() {
        return vista;
    }

    public void setVista(Vista vista) {
        this.vista = vista;
    }

    public Modelo getModelo() {
        return modelo;
    }

    public void setModelo(Modelo modelo) {
        this.modelo = modelo;
    }

    public RegistroUsuarios getRegistroUsuarios() {
        return registroUsuarios;
    }

    public void setRegistroUsuarios(RegistroUsuarios registroUsuarios) {
        this.registroUsuarios = registroUsuarios;
    }

    public Acceso getAcceso() {
        return acceso;
    }

    public void setAcceso(Acceso acceso) {
        this.acceso = acceso;
    }

    public SplashScreen getSplashScreen() {
        return splashScreen;
    }

    public void setSplashScreen(SplashScreen splashScreen) {
        this.splashScreen = splashScreen;
    }

    public boolean isPassFail() {
        return passFail;
    }

    public void setPassFail(boolean passFail) {
        this.passFail = passFail;
    }

    public boolean isUserFail() {
        return userFail;
    }

    public void setUserFail(boolean userFail) {
        this.userFail = userFail;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public CambioContr getCambioContr() {
        return cambioContr;
    }

    public void setCambioContr(CambioContr cambioContr) {
        this.cambioContr = cambioContr;
    }
}

