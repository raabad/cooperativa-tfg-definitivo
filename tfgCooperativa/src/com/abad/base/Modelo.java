package com.abad.base;

import com.abad.gui.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.hibernate.query.Query;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
/**
clase que juntara la clase vista y controlador
 */
public class Modelo {

        private static SessionFactory sessionFactory;
        private static Session session;
        HibernateUtil hibernate;
        boolean containsAdmin;
/**
creación del contructor donde inicializamos las varibales, hibernate y contrains admin.
 */
    public Modelo() {
        hibernate = new HibernateUtil();
        containsAdmin = false;
    }

    /**
     * metodo para conetarse a la base de datos, en nuestro caso será una conexión en cascada.
     */
    public void conectar(){
/**
se construye la sesion, se abre la sesion
 se recorre la lista de usuarios a traves de un foreach
 si el usuario su rol es diferente a uno y no hay admin no puede entrar
 se crea el admin predeterminado en la base de datos.
 y atraves de un if en el que decimos que si la varibale
 contains admin es igual a false de de alta un administrador
 */
        hibernate.buildSessionFactory();
        hibernate.openSession();
       session = hibernate.getCurrentSession();


        List<Usuario> lista = getUsuarios();
        for (Usuario usuario : lista){
            if(usuario.getRol() != 1 && containsAdmin == false){
                containsAdmin = false;
            }
            if(usuario.getRol() == 1){
                containsAdmin = true;
            }
        }
        Usuario admin = new Usuario("admin","admin",1);
        System.out.println(containsAdmin);
        if (containsAdmin == false){
            session.beginTransaction();
            session.save(admin);
            session.getTransaction().commit();

        }
    }

    /**
     * metodo para desconectarse de la base de datos
     * cerramos la sesion
     */

    public void desconectar(){
        hibernate.closeSessionFactory();
    }

    /**
     * damos de alta un usuario, al que le pasaremos por parametro las variables que le corresponda
     * el admin registrara al usario y segun el rol asignado será empleado o visualizador
     * @param nameUser
     * @param pass
     * @param rol
     */

    public void altaUsuario(String nameUser,String pass, String rol){
        Usuario usuario;
        if(rol.equals("Empleado")) {
            usuario = new Usuario(nameUser, pass, 2);
        }
        else{
            usuario = new Usuario(nameUser, pass, 3);
        }

        session.beginTransaction();
        session.save(usuario);
        session.getTransaction().commit();
    }

    /**
     * modificará los datos en la base de datos pasando como parametro el objeto usuario.
     * @param usuario
     */

    public void modificarUsuario(Usuario usuario){
        session.beginTransaction();
        session.saveOrUpdate(usuario);
        session.getTransaction().commit();

    }

    /**
     * eliminara usuarios pasando como parametro el objeto usuario
     * @param usuario
     */
    public void eliminarUsuario(Usuario usuario){
        session.beginTransaction();
        session.delete(usuario);
        session.getTransaction().commit();
    }

    /**
     * damos de alta un agricultor en la base de datos pasando por parametro sus variables.
     * @param nombre
     * @param direccion
     * @param telefono
     * @param fechaNacimiento
     * @param email
     * @param dni
     * @param ss
     */
    public void altaAgricultor(String nombre, String direccion, int telefono, Timestamp fechaNacimiento,
                               String email, String dni, int ss){
        Agricultor agricultor=new Agricultor(nombre,direccion,telefono,fechaNacimiento,email,dni,ss);
        System.out.println(agricultor+"antes transaction");
        session.beginTransaction();
        session.save(agricultor);
        System.out.println(session.save(agricultor)+"save");
        session.getTransaction().commit();

    }

    public void altaAgricultorTest(Agricultor agricultor){
        session.beginTransaction();
        session.save(agricultor);
        System.out.println(session.save(agricultor)+"save");
        session.getTransaction().commit();

    }

    /**
     * modificamos un agricultor en la base de datos pasando por parametro el objeto agriculor
     * @param agricultor
     */
    public void modificarAgricultor(Agricultor agricultor){
        session.beginTransaction();
        session.saveOrUpdate(agricultor);
        session.getTransaction().commit();

    }

    public void modificarAgricultorTest(Agricultor agricultor){
        session.beginTransaction();
        session.saveOrUpdate(agricultor);
        session.getTransaction().commit();

    }
    /**
     * eliminamos un agricultor en la base de datos pasando por parametro el objeto agriculor
     * @param agricultor
     */
    public void eliminarAgricultor(Agricultor agricultor){
        session.beginTransaction();
        session.delete(agricultor);
        session.getTransaction().commit();
    }

    public void eliminarAgricultorTest(Agricultor agricultor){
        session.beginTransaction();
        session.delete(agricultor);
        session.getTransaction().commit();
    }

    /**
     *  damos de alta un empleado en la base de datos pasando por parametro sus variables.
     * @param nombre
     * @param apellidos
     * @param dni
     * @param email
     * @param fechaContratacion
     * @param experiencia
     * @param telefono
     * @param agricultor
     */
    public void altaEmpleados(String nombre, String apellidos, String dni, String email, Timestamp fechaContratacion, int experiencia, String telefono, Agricultor agricultor){
        Empleados empleados=new Empleados(nombre,apellidos,email,dni,fechaContratacion,experiencia,telefono,agricultor);
        agricultor.getEmpleados().add(empleados);
        session.beginTransaction();
        session.save(empleados);
        session.getTransaction().commit();
    }

    /**
     * modificamos un empleado en la base de datos pasando por parametro el objeto empleado
     * @param empleados
     */
    public void modificarEmpleados(Empleados empleados){
        session.beginTransaction();
        session.saveOrUpdate(empleados);
        session.getTransaction().commit();

    }

    /**
     *  eliminamos un empleado en la base de datos pasando por parametro el objeto empleado
     * @param empleados
     */
    public void eliminarEmpleados(Empleados empleados){
        session.beginTransaction();
        session.delete(empleados);
        session.getTransaction().commit();
    }

    /**
     * damos de alta una maquinaria en la base de datos pasando por parametro sus variables.
     * @param modelo
     * @param marca
     * @param matricula
     * @param combustible
     * @param fechaCompra
     * @param averia
     * @param descripcion
     * @param agricultor
     */
    public void altaMaquinaria(String modelo, String marca, String matricula, String combustible, Timestamp fechaCompra, String averia, String descripcion, Agricultor agricultor){
        Maquinaria maquinaria=new Maquinaria(modelo,marca,matricula,combustible,fechaCompra,averia,descripcion,agricultor);
        agricultor.getMaquinaria().add(maquinaria);
        session.beginTransaction();
        session.save(maquinaria);
        session.getTransaction().commit();
    }

    /**
     *  modificamos una maquinaria en la base de datos pasando por parametro sus variables.
     * @param maquinaria
     */
    public void modificarMaquinaria(Maquinaria maquinaria){
        session.beginTransaction();
        session.saveOrUpdate(maquinaria);
        session.getTransaction().commit();

    }

    /**
     * eliminamos una maquinaria en la base de datos pasando por parametro sus variables.
     * @param maquinaria
     */
    public void eliminarMaquinaria(Maquinaria maquinaria){
        session.beginTransaction();
        session.delete(maquinaria);
        session.getTransaction().commit();
    }

    /**
     * damos de alta un cliente en la base de datos pasandole por parametro todas sus variables
     * @param nombre
     * @param apellidos
     * @param dni
     * @param telefono
     * @param fechaNacimiento
     * @param domicilio
     * @param codigoPostal
     * @param agricultor
     */
    public void altaClientes(String nombre, String apellidos, String dni, int telefono, Timestamp fechaNacimiento, String domicilio, String codigoPostal, Agricultor agricultor){
        Cliente cliente=new Cliente(nombre,apellidos,dni,telefono,fechaNacimiento,domicilio,codigoPostal,agricultor);
        agricultor.getCliente().add(cliente);
        session.beginTransaction();
        session.save(cliente);
        session.getTransaction().commit();
    }

    public void altaClientesTest(Cliente cliente){

        session.beginTransaction();
        session.save(cliente);
        session.getTransaction().commit();
    }

    /**
     * modificamos el objeto cliente pasandoselo por parametro
     * @param cliente
     */
    public void modificarClientes(Cliente cliente){
        session.beginTransaction();
        session.saveOrUpdate(cliente);
        session.getTransaction().commit();

    }

    /**
     * eliminamos el objeto cliente pasandoselo por parametro
     * @param cliente
     */
    public void eliminarClientes(Cliente cliente){
        session.beginTransaction();
        session.delete(cliente);
        session.getTransaction().commit();
    }

    /**
     * damos de alta un producto en la base de datos pasandole por parametro todas sus variables
     * @param nombre
     * @param fechaCosecha
     * @param precio
     * @param descripcion
     * @param humedad
     * @param cantidad
     * @param agricultor
     */
    public void altaProductos(String nombre, Timestamp fechaCosecha, float precio, String descripcion, float humedad, int cantidad, Agricultor agricultor){
        Productos productos=new Productos(nombre,fechaCosecha,precio,descripcion,humedad,cantidad,agricultor);
        agricultor.getProductos().add(productos);
        session.beginTransaction();
        session.save(productos);
        session.getTransaction().commit();
    }

    /**
     * modificamos un producto en la base de datos pasandoselo por parametro
     * @param productos
     */

    public void modificarProductos(Productos productos){
        session.beginTransaction();
        session.saveOrUpdate(productos);
        session.getTransaction().commit();

    }

    /**
     * eliminamos un producto de la base de datos pasandoselo por parametro
     * @param productos
     */

    public void eliminarProductos(Productos productos){
        session.beginTransaction();
        session.delete(productos);
        session.getTransaction().commit();
    }

    /**
     * buscamos un usuario en la base de datos
     * @return
     */

    public List<Usuario> getUsuarios(){
        Query query = session.createQuery("FROM Usuario ");
        List<Usuario> lista = query.getResultList();
        return lista;
    }

    /**
     * buscamos un agricultor en la base de datos
     * @return
     */
    public List<Agricultor> getAgricultor(){
        Query query = session.createQuery("FROM Agricultor ");
        List<Agricultor> lista = query.getResultList();
        return lista;
    }

    /**
     * buscamos un empleado en la base de datos
     * @return
     */
    public List<Empleados> getEmpleados(){
        Query query = session.createQuery("FROM Empleados ");
        List<Empleados> lista = query.getResultList();
        return lista;
    }

    /**
     * buscamos una maquinaria en la base de datos
     * @return
     */
    public List<Maquinaria> getMaquinaria(){
        Query query = session.createQuery("FROM Maquinaria ");
        List<Maquinaria> lista = query.getResultList();
        return lista;
    }

    /**
     * buscamos un cliente en la base de datos
     * @return
     */
    public List<Cliente> getCliente(){
        Query query = session.createQuery("FROM Cliente ");
        List<Cliente> lista = query.getResultList();
        return lista;
    }

    /**
     * buscamos un producto en la base de datos
     * @return
     */
    public List<Productos> getProductos(){
        Query query = session.createQuery("FROM Productos ");
        List<Productos> lista = query.getResultList();
        return lista;
    }

    /**
     * devolvemos un agricultor seleccionado en la base de datos empleados para buscar los empleados que le pertencen
     * @param agricultorSeleccionado
     * @return
     */
    public ArrayList<Empleados> getEmpleadosAgricultor(Agricultor agricultorSeleccionado) {

        Query query = session.createQuery("FROM Empleados WHERE agricultor = :prop");
        query.setParameter("prop", agricultorSeleccionado);
        ArrayList<Empleados> lista = (ArrayList<Empleados>) query.getResultList();

        return lista;
    }

    /**
     * devolvemos un agricultor seleccionado en la base de datos Productos para buscar los productos que le pertencen
     * @param agricultorSeleccionado
     * @return
     */
    public ArrayList<Productos> getProductosAgricultor(Agricultor agricultorSeleccionado) {

        Query query = session.createQuery("FROM Productos WHERE agricultor = :prop");
        query.setParameter("prop", agricultorSeleccionado);
        ArrayList<Productos> lista = (ArrayList<Productos>) query.getResultList();

        return lista;
    }

    /**
     * devolvemos un agricultor seleccionado en la base de datos Clientes para buscar los Clientes que le pertencen
     * @param agricultorSeleccionado
     * @return
     */
    public ArrayList<Cliente>getClienteAgricultor(Agricultor agricultorSeleccionado){

        Query query = session.createQuery("FROM Cliente WHERE agricultor = :prop");
        query.setParameter("prop", agricultorSeleccionado);
        ArrayList<Cliente> lista = (ArrayList<Cliente>) query.getResultList();

        return lista;
    }

    /**
     * devolvemos un agricultor seleccionado en la base de datos Maquinaria para buscar la maquinaria que le pertencen
     * @param agricultorSeleccionado
     * @return
     */
    public ArrayList<Maquinaria>getMaquinariaAgricultor(Agricultor agricultorSeleccionado){

       Query query = session.createQuery("FROM Maquinaria WHERE agricultor = :prop");
        query.setParameter("prop", agricultorSeleccionado);
        ArrayList<Maquinaria> lista = (ArrayList<Maquinaria>) query.getResultList();

        return lista;
    }


}
