package com.abad.base;

public class Principal {


    public static void main(String[] args) {
        RegistroUsuarios registroUsuarios=new RegistroUsuarios();
        Vista vista= new Vista();
        Modelo modelo=new Modelo();
        Acceso acceso=new Acceso();
        Controlador controlador=new Controlador( vista,  modelo, registroUsuarios, acceso);
    }


}
