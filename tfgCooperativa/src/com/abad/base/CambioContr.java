package com.abad.base;

import com.abad.gui.Usuario;

import javax.swing.*;
import java.awt.event.*;

/**
 * creacion del jdialog para cambiar la contraseña de los usuarios
 */
public class CambioContr extends JDialog {
     JPanel contentPane;
     JPasswordField passwordContrAct;
     JPasswordField passwordContrNueva;
     JPasswordField passwordRepiteContr;
     JButton btnAceptarCambio;

    Usuario usuario;
    Modelo modelo;

    /**
     * creamos el constructor
     * @param usuario
     * @param modelo
     */
    public CambioContr(Usuario usuario, Modelo modelo) {
        this.usuario = usuario;
        this.modelo = modelo;
        inicializar();
    }

    /**
     * incializamos botones
     */
    public void inicializar() {
        setContentPane(contentPane);
        setModal(true);
        setSize(600,300);
        getRootPane().setDefaultButton(btnAceptarCambio);
/**
Para cambiar la contraseña, creamos un condicional, en el que le digamos que el usuario tenga la misma contrasenya
a la introducida, si se cumple la condicion, le decimos que cumpla la condicion de que el campo de contraseña nueva y repetir contraseña
sea iguales y si son iguales, setamos la contraseña nueva y la modificamos.
 */
        btnAceptarCambio.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                System.out.println("entra");
                if(usuario.getPass().equals(new String(passwordContrAct.getPassword()))){
                    System.out.println("hola");
                    if(new String(passwordContrNueva.getPassword()).equals(new String(passwordRepiteContr.getPassword()))){
                        System.out.println("llega cambio contr");
                        usuario.setPass(new String(passwordContrNueva.getPassword()));
                        modelo.modificarUsuario(usuario);
                        JOptionPane.showMessageDialog(null, "CONTRASEÑA CAMBIADA", "CAMBIO CONTRASEÑA", JOptionPane.INFORMATION_MESSAGE);
                        dispose();
                    }
                }
                else{
                    JOptionPane.showMessageDialog(null, "CONTRASEÑA INCORRECTA", "ERROR", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
/*
Hacemos visible la vista
 */
        setVisible(true);


    }



}
