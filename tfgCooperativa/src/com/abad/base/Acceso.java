package com.abad.base;

import javax.swing.*;

/**
 * ventana de acceso al programa
 */
public class Acceso {
     JPanel panel1;
     JTextField textUsuarioInicio;
     JPasswordField passwordInicio;
     JButton btnAcceder;
    JFrame frame;

    /**
     * creación del constructor
     */
    public Acceso() {
         frame = new JFrame("Acceso");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(300,150);
        frame.setVisible(false);
        ImageIcon logo = new ImageIcon("Imagenes/IconoFondoNegro.png");
        frame.setIconImage(logo.getImage());
        frame.setLocationRelativeTo(null);
    }


}
