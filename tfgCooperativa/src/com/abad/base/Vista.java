package com.abad.base;

import com.abad.gui.*;
import com.github.lgooddatepicker.components.DateTimePicker;

import javax.swing.*;
/*
creamos botones,lista, textfield,comboBox,items...
 */
public class Vista {
     JTabbedPane tabbedPane1;
     JPanel panel1;
     JTextField textNombreAgr;
     JTextField textDireccionAgr;
     JTextField textTelfAgr;
     JTextField textEmailAgr;
     JTextField textDniAgr;
     JTextField textSSAgr;
     JButton btnEliminarAgr;
     JButton btnModAgr;
     JButton btnAltaAgr;
     JList listAgricultor;
     JTextField textNombreEmp;
     JTextField textApellidosEmp;
     JTextField textDniEmp;
     JTextField textEMailEmp;
     JTextField textFieldExperienciaEmp;
     JTextField textTelfEmp;
     JList listEmp;
     JButton btnAltaEmp;
     JButton btnModEmp;
     JButton btnBajaEmp;
     JTextField textModeloMaq;
     JTextField textMarcaMaq;
     JTextField textMatriculaMaq;
     JTextField textAveriaMaq;
     JButton btnAltaMaq;
     JButton btnModMaq;
     JButton btnEliminarMaq;
     JList listMaq;
     JTextField textNombreCliente;
     JTextField textApellidosCliente;
     JTextField textDniCliente;
     JTextField textTelfCliente;
     JTextField textDomicilioCliente;
     JTextField textCodCliente;
     JList listClientes;
     JButton btnAltaCl;
     JButton btnModCl;
     JButton btnBajaCl;
     JTextField textNomProd;
     JTextField textPrecioProd;
     JTextField textHumedadProd;
     JTextField textCantProd;
     JButton btnAltaProd;
     JButton btnModProd;
     JButton btnBajaProd;
     JList listProd;
     JComboBox cbAgricultorProd;
     JComboBox cbAgricultorClient;
     JComboBox cbAgricultorMaq;
     JComboBox cbAgricultorEmp;
     DateTimePicker dpAgr;
     DateTimePicker dpEmp;
     JComboBox cbCombustibleMaq;
     JTextArea textAreaMaq;
     DateTimePicker dpCliente;
     DateTimePicker dpProd;
     JTextArea textAreaDescrProd;
     JTextField textBuscarAgr;
     JTextField textBuscarEmp;
     JTextField textBuscarMaq;
     JTextField textBuscarCl;
     JTextField textBuscarProd;
     DateTimePicker dpMaq;
     JList listEmpleadosAgr;
     JList listClientesAgr;
     JList listMaquinariaAgr;
     JList listProductosAgr;
     JButton btnInformeCliente;
     JButton btnInformeEmpleados;
     JButton btnInformeProductos;
     JButton btnInformeMaq;
     JButton btnInformeAgricultor;
    JFrame frame;
      JMenuBar barra;
      JMenu menuSalir;
      JMenu menuAdmin;
      JMenu menuContrasenya;
      JMenuItem itemContrasenya;
      JMenuItem itemSalir;
      JMenuItem itemAdmin;
      DefaultListModel<Agricultor> dlmAgr;
      DefaultListModel<Cliente> dlmCl;
      DefaultListModel<Empleados> dlmEmp;
      DefaultListModel<Productos> dlmProd;
      DefaultListModel<Maquinaria> dlmMaq;

      DefaultComboBoxModel <Agricultor>dcbmAgrProd;
      DefaultComboBoxModel <Agricultor>dcbmAgrCl;
      DefaultComboBoxModel <Agricultor>dcbmAgrEmp;
      DefaultComboBoxModel <Agricultor>dcbmAgrMaq;

      DefaultListModel dlmClAgr;
      DefaultListModel dlmEmpAgr;
      DefaultListModel dlmProdAgr;
      DefaultListModel dlmMaqAgr;


/**
creamos el constructor de la vista
 */
    public Vista() {

       frame = new JFrame("AGROABAD");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(false);
        ImageIcon logo = new ImageIcon("Imagenes/IconoFondoNegro.png");
        frame.setIconImage(logo.getImage());
        frame.setLocationRelativeTo(null);
        inicializar();
        crearMenu();
    }

    /**
     * creamos el menu de contraseñas, gestion de usuarios y cambios de contraseña
     */
     private void crearMenu() {

          barra = new JMenuBar();
          menuContrasenya = new JMenu("Contraseña");
          menuAdmin = new JMenu(" Usuarios");
          menuSalir = new JMenu("Salir");

          itemContrasenya = new JMenuItem("Cambiar Contraseña");

          itemContrasenya.setActionCommand("Cambiar Contr");

          itemAdmin = new JMenuItem(" Registrar Usuarios");

          itemAdmin.setActionCommand("Registrar Usuarios");

          itemSalir = new JMenuItem("Desconectar");

          itemSalir.setActionCommand("Salir");

          menuAdmin.add(itemAdmin);
          menuSalir.add(itemSalir);
          menuContrasenya.add(itemContrasenya);

          barra.add(menuSalir);
          barra.add(menuAdmin);
          barra.add(menuContrasenya);

          frame.setJMenuBar(barra);
     }
/**
inicilizamos los modelos y comboBox
 */
     private void inicializar() {

         dlmAgr=new DefaultListModel<>();
         listAgricultor.setModel(dlmAgr);

         dlmCl=new DefaultListModel<>();
         listClientes.setModel(dlmCl);

         dlmEmp=new DefaultListModel<>();
         listEmp.setModel(dlmEmp);

         dlmProd=new DefaultListModel<>();
         listProd.setModel(dlmProd);

         dlmMaq=new DefaultListModel<>();
         listMaq.setModel(dlmMaq);

         dcbmAgrCl=new DefaultComboBoxModel<>();
         cbAgricultorClient.setModel(dcbmAgrCl);

         dcbmAgrProd=new DefaultComboBoxModel<>();
         cbAgricultorProd.setModel(dcbmAgrProd);


         dcbmAgrEmp=new DefaultComboBoxModel<>();
         cbAgricultorEmp.setModel(dcbmAgrEmp);

         dcbmAgrMaq=new DefaultComboBoxModel<>();
         cbAgricultorMaq.setModel(dcbmAgrMaq);

         dlmClAgr=new DefaultListModel();
         listClientesAgr.setModel(dlmClAgr);
         dlmEmpAgr=new DefaultListModel();
         listEmpleadosAgr.setModel(dlmEmpAgr);
         dlmMaqAgr=new DefaultListModel();
         listMaquinariaAgr.setModel(dlmMaqAgr);
         dlmProdAgr=new DefaultListModel();
         listProductosAgr.setModel(dlmProdAgr);



     }


}
