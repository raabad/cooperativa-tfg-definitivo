package com.abad.base;

import com.abad.gui.Usuario;

import javax.swing.*;
/**
creamos la venta de registro de usuarios
 */
public class RegistroUsuarios {
     JPanel panel1;
     JTextField textUserRegistro;
     JPasswordField passwordContrRegistro;
     JComboBox cbRegUs;
     JButton btnAltaUsReg;
     JButton btnBajaUsReg;
     JButton btnModUsReg;
     JList listUsuariosReg;
    JFrame frame;
/**
se crean los modelos
 */
     DefaultListModel<Usuario> dlmUsuarios;
     DefaultComboBoxModel<String> dcbmRol;
/**
se crea el constructor
 */
    public RegistroUsuarios() {

         frame = new JFrame("Registro Usuarios");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setSize(600,320);
        frame.setVisible(false);
        frame.setLocationRelativeTo(null);
        ImageIcon logo = new ImageIcon("Imagenes/IconoFondoNegro.png");
        frame.setIconImage(logo.getImage());

        inicializar();

    }
/*
incializamos los modelos
 */
    private void inicializar() {
        dlmUsuarios=new DefaultListModel<>();
        listUsuariosReg.setModel(dlmUsuarios);

        dcbmRol=new DefaultComboBoxModel<>();
        cbRegUs.setModel(dcbmRol);

        dcbmRol.addElement("Empleado");
        dcbmRol.addElement("Visualizador");
    }


}
