package com.abad.base;

import com.abad.gui.Agricultor;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class PruebasUnitariasControlador extends Controlador {
    private static Agricultor agricultor;
    private Vista vista;
    private Modelo modelo;
    private Controlador controlador;
    private  Acceso acceso;
    private SplashScreen splashScreen;
    private CambioContr cambioContr;
    private RegistroUsuarios registroUsuarios;

    public PruebasUnitariasControlador(){
        this.vista = new Vista();
        this.modelo = new Modelo();
        this.acceso = new Acceso();
        this.registroUsuarios = new RegistroUsuarios();
        this.controlador = new Controlador( vista,  modelo, registroUsuarios, acceso);
    }

    @Before
    public void setUp() throws Exception {
       agricultor=new Agricultor("raul","calle",111,Timestamp.valueOf(LocalDateTime.now()),"a","222",2222);
    }

    @Test
    public void visualizarAgricutlor(){
        vista.textNombreAgr.setText("" + agricultor.getNombre());
        vista.textDireccionAgr.setText("" + agricultor.getDireccion());
        vista.textTelfAgr.setText("" + agricultor.getTelefono());
        vista.dpAgr.setDateTimePermissive(agricultor.getFechaNacimiento().toLocalDateTime());
        vista.textEmailAgr.setText("" + agricultor.getEmail());
        vista.textDniAgr.setText("" + agricultor.getDni());
        Assert.assertTrue("se ha visualizado correctamente",vista.textNombreAgr.getText().equals("raul"));
    }

    @Test
    public void iniciarAcceso(){
        acceso.textUsuarioInicio.setText("admin");
        acceso.passwordInicio.setText("admin");
        controlador.getAcceso().btnAcceder.doClick();
    }
}
