package com.abad.base;

import com.abad.gui.*;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

public class PruebasUnitariasModelo extends Modelo {

    private static Usuario usuario;
    private static Cliente cliente;
    private static Agricultor agricultor=new Agricultor("raul","calle",111, Timestamp.valueOf(LocalDateTime.now()),"a","222",2222);
    private static Empleados empleados;
    private static Productos productos;
    private static Maquinaria maquinaria;

    @BeforeClass
    public static void setUpClass(){
        usuario = new Usuario("raul","raul",3);
        cliente = new Cliente("cliente","cliente","aaa",222,Timestamp.valueOf(LocalDateTime.now()),"bbb","400",null);
        empleados = new Empleados("emp","emp","22c","aa@",Timestamp.valueOf(LocalDateTime.now()),23,"22",null);
        productos = new Productos("prod",Timestamp.valueOf(LocalDateTime.now()),23,"aaaa",25,1,null);
        maquinaria = new Maquinaria("maq","aa","CQX","Diesel",Timestamp.valueOf(LocalDateTime.now()),"aa","cddf",null);

    }

    @Test
    public void conectar() {
        this.conectar();
        Assert.assertEquals(true, HibernateUtil.getSession().isOpen());
    }
    @Test
    public void altaAgricultor() {

        List lista = this.getAgricultor();
        int cuenta = lista.size();
        this.altaAgricultorTest(agricultor);

        lista = this.getAgricultor();
        Assert.assertTrue("Se ha añadido", cuenta + 1 == lista.size());
        eliminarAgricultor(agricultor);
    }

    @Test
    public void altaCliente() {
        altaAgricultorTest(agricultor);
        List lista = this.getCliente();
        int cantidad = lista.size();
        this.altaClientesTest(cliente);

        lista = this.getCliente();
        Assert.assertTrue("Se ha insertado correctamente", cantidad + 1 == lista.size());
        eliminarAgricultor(agricultor);
        eliminarClientes(cliente);
    }
    @Test
    public void altaClienteEquals(){
        List lista = this.getCliente();
        int cantidad = lista.size();
        altaClientesTest(cliente);
        lista = this.getCliente();
        Assert.assertEquals(cantidad+1,lista.size());
        eliminarClientes(cliente);
    }
    @Test
    public void eliminarClienteNegativo(){
        eliminarClientes(cliente);
        List lista = getCliente();
        Assert.assertFalse(lista.contains(cliente));
    }

    @Test
    public void eliminarClienteAfirmativo() {
        altaClientesTest(cliente);
        int cantidad = getCliente().size();
        eliminarClientes(cliente);
        Assert.assertTrue("El mecanico se ha eliminado correctamente",getCliente().size() == (cantidad-1));
    }

    @Test
    public void eliminarClienteIgual() {
        int cantidadAntes = getCliente().size();
        altaClientesTest(cliente);
        eliminarClientes(cliente);
        int cantidadDespues = getCliente().size();
        Assert.assertEquals(cantidadAntes,cantidadDespues);
    }


    @Test
    public void modificarAgricultor() {
        altaAgricultorTest(agricultor);
        agricultor.setDireccion("aqui");
        modificarAgricultorTest(agricultor);
        Assert.assertEquals("prueba",agricultor.getNombre());
        eliminarAgricultorTest(agricultor);
    }



}
