package com.abad.base;

import javax.swing.*;
import java.awt.*;
/**
clase de creacion de la ventana de carga
 */
public class SplashScreen extends JFrame implements Runnable {

    private JLabel imagen;
    Modelo modelo;
    Thread hilo;
/**
creamos el constructor y le pasamos por parametros el modelo
 */
    public SplashScreen(Modelo modelo) {
        this.modelo=modelo;
        imagen=new JLabel(new ImageIcon("Imagenes/logo.png"));
    }
/**
creacion de un metodo run para crear un hilo y darle un tiempo a la ventana de carga
 */
    @Override
    public void run() {

        this.setContentPane(imagen);
        this.setUndecorated(true);
        this.setIconImage(new ImageIcon("Imagenes/logo.png").getImage());
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        modelo.conectar();
        try {
            hilo.sleep(1500);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.dispose();

    }
}
