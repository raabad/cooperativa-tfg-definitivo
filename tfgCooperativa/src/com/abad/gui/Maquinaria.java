package com.abad.gui;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
public class Maquinaria {
    private Integer id;
    private String modelo;
    private String marca;
    private String matricula;
    private String combustible;
    private Timestamp fechaCompra;
    private String averia;
    private String descripcion;
    private Agricultor agricultor;

    public Maquinaria(String modelo, String marca, String matricula, String combustible, Timestamp fechaCompra, String averia, String descripcion, Agricultor agricultor) {
        this.modelo = modelo;
        this.marca = marca;
        this.matricula = matricula;
        this.combustible = combustible;
        this.fechaCompra = fechaCompra;
        this.averia = averia;
        this.descripcion = descripcion;
        this.agricultor = agricultor;
    }

    public Maquinaria() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
   // @Basic
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "modelo")
    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    @Basic
    @Column(name = "marca")
    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    @Basic
    @Column(name = "matricula")
    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    @Basic
    @Column(name = "combustible")
    public String getCombustible() {
        return combustible;
    }

    public void setCombustible(String combustible) {
        this.combustible = combustible;
    }

    @Basic
    @Column(name = "fecha_compra")
    public Timestamp getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(Timestamp fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    @Basic
    @Column(name = "averia")
    public String getAveria() {
        return averia;
    }

    public void setAveria(String averia) {
        this.averia = averia;
    }

    @Basic
    @Column(name = "descripcion")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Maquinaria that = (Maquinaria) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(modelo, that.modelo) &&
                Objects.equals(marca, that.marca) &&
                Objects.equals(matricula, that.matricula) &&
                Objects.equals(combustible, that.combustible) &&
                Objects.equals(fechaCompra, that.fechaCompra) &&
                Objects.equals(averia, that.averia) &&
                Objects.equals(descripcion, that.descripcion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, modelo, marca, matricula, combustible, fechaCompra, averia, descripcion);
    }

    @ManyToOne
    @JoinColumn(name = "idAgricultor", referencedColumnName = "id")
    public Agricultor getAgricultor() {
        return agricultor;
    }

    public void setAgricultor(Agricultor agricultor) {
        this.agricultor = agricultor;
    }

    @Override
    public String toString() {
        return "Maquinaria{" +
                "modelo='" + modelo + '\'' +
                ", marca='" + marca + '\'' +
                ", matricula='" + matricula + '\'' +
                ", combustible='" + combustible + '\'' +
                ", fechaCompra=" + fechaCompra +
                ", averia='" + averia + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", agricultor=" + agricultor +
                '}';
    }
}
