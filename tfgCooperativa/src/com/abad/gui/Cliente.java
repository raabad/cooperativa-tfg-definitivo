package com.abad.gui;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Cliente {
    private int id;
    private String nombre;
    private String apellidos;
    private String dni;
    private int telefono;
    private Timestamp fechaNacimiento;
    private String domicilio;
    private String codigoPostal;
    private Agricultor agricultor;
    private List<Productos> productos;

    public Cliente( String nombre, String apellidos, String dni, int telefono, Timestamp fechaNacimiento, String domicilio, String codigoPostal, Agricultor agricultor) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.dni = dni;
        this.telefono = telefono;
        this.fechaNacimiento = fechaNacimiento;
        this.domicilio = domicilio;
        this.codigoPostal = codigoPostal;
        this.agricultor = agricultor;
        productos=new ArrayList<>();
    }

    public Cliente() {
        productos=new ArrayList<>();
    }



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "apellidos")
    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Basic
    @Column(name = "dni")
    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    @Basic
    @Column(name = "telefono")
    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    @Basic
    @Column(name = "fecha_nacimiento")
    public Timestamp getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Timestamp fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Basic
    @Column(name = "domicilio")
    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    @Basic
    @Column(name = "codigo_postal")
    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cliente cliente = (Cliente) o;
        return id == cliente.id &&
                telefono == cliente.telefono &&
                Objects.equals(nombre, cliente.nombre) &&
                Objects.equals(apellidos, cliente.apellidos) &&
                Objects.equals(dni, cliente.dni) &&
                Objects.equals(fechaNacimiento, cliente.fechaNacimiento) &&
                Objects.equals(domicilio, cliente.domicilio) &&
                Objects.equals(codigoPostal, cliente.codigoPostal);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, apellidos, dni, telefono, fechaNacimiento, domicilio, codigoPostal);
    }

    @ManyToOne
    @JoinColumn(name = "idAgricultor", referencedColumnName = "id")
    public Agricultor getAgricultor() {
        return agricultor;
    }

    public void setAgricultor(Agricultor agricultor) {
        this.agricultor = agricultor;
    }

    @ManyToMany
    @JoinTable(name = "clientes_productos", catalog = "", schema = "cooperativa", joinColumns = @JoinColumn(name = "id_clientes", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_productos", referencedColumnName = "id", nullable = false))
    public List<Productos> getProductos() {
        return productos;
    }

    public void setProductos(List<Productos> productos) {
        this.productos = productos;
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "nombre='" + nombre + '\'' +
                ", apellidos='" + apellidos + '\'' +
                ", dni='" + dni + '\'' +
                ", telefono=" + telefono +
                ", fechaNacimiento=" + fechaNacimiento +
                ", domicilio='" + domicilio + '\'' +
                ", codigoPostal='" + codigoPostal + '\'' +
                ", agricultor=" + agricultor +
                '}';
    }
}
