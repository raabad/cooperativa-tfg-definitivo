package com.abad.gui;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Agricultor {
    private int id;
    private String nombre;
    private String direccion;
    private int telefono;
    private Timestamp fechaNacimiento;
    private String email;
    private String dni;
    private int ss;
    private List<Cliente> cliente;
    private List<Maquinaria> maquinaria;
    private List<Empleados> empleados;
    private List<Productos> productos;

    public Agricultor( String nombre, String direccion, int telefono, Timestamp fechaNacimiento,
                      String email, String dni, int ss) {

        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.fechaNacimiento = fechaNacimiento;
        this.email = email;
        this.dni = dni;
        this.ss = ss;
        cliente=new ArrayList<>();
        empleados=new ArrayList<>();
        maquinaria=new ArrayList<>();
        productos=new ArrayList<>();
    }

    public Agricultor() {
        cliente=new ArrayList<>();
        empleados=new ArrayList<>();
        maquinaria=new ArrayList<>();
        productos=new ArrayList<>();
    }

    @Id
    //persistencia en cascada
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "direccion")
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Basic
    @Column(name = "telefono")
    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    @Basic
    @Column(name = "fechaNacimiento")
    public Timestamp getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Timestamp fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "DNI")
    public String getDni() {
        return dni;
    }

    public void setDni(String  dni) {
        this.dni = dni;
    }

    @Basic
    @Column(name = "SS")
    public int getSs() {
        return ss;
    }

    public void setSs(int ss) {
        this.ss = ss;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Agricultor that = (Agricultor) o;
        return id == that.id &&
                telefono == that.telefono &&
                dni == that.dni &&
                ss == that.ss &&
                Objects.equals(nombre, that.nombre) &&
                Objects.equals(direccion, that.direccion) &&
                Objects.equals(fechaNacimiento, that.fechaNacimiento) &&
                Objects.equals(email, that.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, direccion, telefono, fechaNacimiento, email, dni, ss);
    }

    @OneToMany(mappedBy = "agricultor")
    public List<Cliente> getCliente() {
        return cliente;
    }

    public void setCliente(List<Cliente> cliente) {
        this.cliente = cliente;
    }

    @OneToMany(mappedBy = "agricultor")
    public List<Maquinaria> getMaquinaria() {
        return maquinaria;
    }

    public void setMaquinaria(List<Maquinaria> maquinaria) {
        this.maquinaria = maquinaria;
    }

    @OneToMany(mappedBy = "agricultor")
    public List<Empleados> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(List<Empleados> empleados) {
        this.empleados = empleados;
    }

    @OneToMany(mappedBy = "agricultor")
    public List<Productos> getProductos() {
        return productos;
    }

    public void setProductos(List<Productos> productos) {
        this.productos = productos;
    }

    @Override
    public String toString() {
        return "Agricultor{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", telefono=" + telefono +
                ", email='" + email + '\'' +
                ", dni='" + dni + '\'' +

                '}';
    }
}
