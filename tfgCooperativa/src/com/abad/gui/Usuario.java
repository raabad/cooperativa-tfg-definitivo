package com.abad.gui;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Usuario {
    private int id;
    private String nameUser;
    private String pass;
    private Integer rol;

    public Usuario() {
    }

    public Usuario( String nameUser, String pass, Integer rol) {
        this.nameUser = nameUser;
        this.pass = pass;
        this.rol = rol;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name_user")
    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    @Basic
    @Column(name = "pass")
    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    @Basic
    @Column(name = "rol")
    public Integer getRol() {
        return rol;
    }

    public void setRol(Integer rol) {
        this.rol = rol;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Usuario usuario = (Usuario) o;
        return id == usuario.id &&
                Objects.equals(nameUser, usuario.nameUser) &&
                Objects.equals(pass, usuario.pass) &&
                Objects.equals(rol, usuario.rol);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nameUser, pass, rol);
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "nameUser='" + nameUser + '\'' +
                ", pass='" + pass + '\'' +
                ", rol=" + rol +
                '}';
    }
}
