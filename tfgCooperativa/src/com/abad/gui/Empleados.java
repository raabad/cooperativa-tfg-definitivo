package com.abad.gui;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
public class Empleados {
    private int id;
    private String nombre;
    private String apellidos;
    private String dni;
    private String email;
    private Timestamp fechaContratacion;
    private int experiencia;
    private String telefono;
    private Agricultor agricultor;

    public Empleados( String nombre, String apellidos, String dni, String email, Timestamp fechaContratacion, int experiencia, String telefono, Agricultor agricultor) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.dni = dni;
        this.email = email;
        this.fechaContratacion = fechaContratacion;
        this.experiencia = experiencia;
        this.telefono = telefono;
        this.agricultor = agricultor;
    }

    public Empleados() {

    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "apellidos")
    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Basic
    @Column(name = "dni")
    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "fecha_contratacion")
    public Timestamp getFechaContratacion() {
        return fechaContratacion;
    }

    public void setFechaContratacion(Timestamp fechaContratacion) {
        this.fechaContratacion = fechaContratacion;
    }

    @Basic
    @Column(name = "experiencia")
    public int getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(int experiencia) {
        this.experiencia = experiencia;
    }

    @Basic
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Empleados empleados = (Empleados) o;
        return id == empleados.id &&
                experiencia == empleados.experiencia &&
                Objects.equals(nombre, empleados.nombre) &&
                Objects.equals(apellidos, empleados.apellidos) &&
                Objects.equals(dni, empleados.dni) &&
                Objects.equals(email, empleados.email) &&
                Objects.equals(fechaContratacion, empleados.fechaContratacion) &&
                Objects.equals(telefono, empleados.telefono);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, apellidos, dni, email, fechaContratacion, experiencia, telefono);
    }

    @ManyToOne
    @JoinColumn(name = "idAgricultor", referencedColumnName = "id")
    public Agricultor getAgricultor() {
        return agricultor;
    }

    public void setAgricultor(Agricultor agricultor) {
        this.agricultor = agricultor;
    }

    @Override
    public String toString() {
        return "Empleados{" +
                "nombre='" + nombre + '\'' +
                ", apellidos='" + apellidos + '\'' +
                ", dni='" + dni + '\'' +
                ", email='" + email + '\'' +
                ", fechaContratacion=" + fechaContratacion +
                ", experiencia=" + experiencia +
                ", telefono='" + telefono + '\'' +
                ", agricultor=" + agricultor +
                '}';
    }
}
