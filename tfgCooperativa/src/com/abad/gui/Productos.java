package com.abad.gui;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Productos {
    private int id;
    private String nombre;
    private Timestamp fechaCosecha;
    private float precio;
    private String descripcion;
    private float humedad;
    private int cantidad;
    private Agricultor agricultor;
    private List<Cliente> clientes;

    public Productos( String nombre, Timestamp fechaCosecha, float precio, String descripcion, float humedad, int cantidad, Agricultor agricultor) {

        this.nombre = nombre;
        this.fechaCosecha = fechaCosecha;
        this.precio = precio;
        this.descripcion = descripcion;
        this.humedad = humedad;
        this.cantidad = cantidad;
        this.agricultor = agricultor;
        clientes=new ArrayList<>();
    }

    public Productos() {
        clientes=new ArrayList<>();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "fecha_cosecha")
    public Timestamp getFechaCosecha() {
        return fechaCosecha;
    }

    public void setFechaCosecha(Timestamp fechaCosecha) {
        this.fechaCosecha = fechaCosecha;
    }

    @Basic
    @Column(name = "precio")
    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    @Basic
    @Column(name = "descripcion")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Basic
    @Column(name = "humedad")
    public float getHumedad() {
        return humedad;
    }

    public void setHumedad(float humedad) {
        this.humedad = humedad;
    }

    @Basic
    @Column(name = "cantidad")
    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Productos productos = (Productos) o;
        return id == productos.id &&
                Double.compare(productos.precio, precio) == 0 &&
                Double.compare(productos.humedad, humedad) == 0 &&
                cantidad == productos.cantidad &&
                Objects.equals(nombre, productos.nombre) &&
                Objects.equals(fechaCosecha, productos.fechaCosecha) &&
                Objects.equals(descripcion, productos.descripcion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, fechaCosecha, precio, descripcion, humedad, cantidad);
    }

    @ManyToOne
    @JoinColumn(name = "idAgricultor", referencedColumnName = "id")
    public Agricultor getAgricultor() {
        return agricultor;
    }

    public void setAgricultor(Agricultor agricultor) {
        this.agricultor = agricultor;
    }

    @ManyToMany(mappedBy = "productos")
    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }

    @Override
    public String toString() {
        return "Productos{" +
                "nombre='" + nombre + '\'' +
                ", fechaCosecha=" + fechaCosecha +
                ", precio=" + precio +
                ", descripcion='" + descripcion + '\'' +
                ", humedad=" + humedad +
                ", cantidad=" + cantidad +
                ", agricultor=" + agricultor +
                '}';
    }
}
